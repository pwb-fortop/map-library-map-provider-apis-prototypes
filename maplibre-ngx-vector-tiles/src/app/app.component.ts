import { Component, OnInit } from '@angular/core';
import { Map, MapMouseEvent } from 'maplibre-gl';

@Component({
  selector: 'app-root',
  template: `
    <mgl-map
      [style]="'/assets/osm_liberty.json'"
      [zoom]="[9]"
      [center]="[5.2793703, 52.2129919]"
      [minZoom]="7"
      (mapLoad)="onMapLoad($event)"
    >
      <mgl-marker [lngLat]="[5.2793703, 52.2129919]">
        <div (click)="handleClick($event)" class="marker">Foutmelding</div>
      </mgl-marker>
    </mgl-map>
  `,
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'Maplibre-ngx-vector-tiles';
  map!: Map;

  handleClick(event: MouseEvent) {
    console.log('Gebruikelijk zou je nu naar de detail pagina worden verstuurd.')
  }

  onMapLoad(map: Map): void {
    map.on('click', (event: MapMouseEvent) => {
      console.log('Location: ', event.lngLat.lat, event.lngLat.lng);
    })

    map.loadImage(
      'assets/fortop.jpg',
      (error, image) => {

        if (image) {
          map.addImage('custom-marker', image);
        }

        map.addSource('conferences', {
          type: 'geojson',
          data: {
            "type": "FeatureCollection",
            "features": [{
              "type": "Feature",
              "properties": {
                "name": "Fortop DCEM"
              },
              "geometry": {
                "type": "Point",
                "coordinates": [
                  6.086111619062052, 52.64093751186624,
                ]
              }
            },
            {
              "type": "Feature",
              "properties": {
                "name": "Fortop DCEM 2"
              },
              "geometry": {
                "type": "Point",
                "coordinates": [
                  6.086577721918474, 52.641592971913354,
                ]
              }
            },
            {
              "type": "Feature",
              "properties": {
                "name": "Bushalte Hasselt"
              },
              "geometry": {
                "type": "Point",
                "coordinates": [
                  6.090190056357301, 52.593402013355956,
                ]
              }
            }],
            cluster: true,
            clusterMaxZoom: 14, // Max zoom to cluster points on
            clusterRadius: 50
          }
        });

        // Add a symbol layer
        map.addLayer({
          'id': 'conferences',
          'type': 'symbol',
          'source': 'conferences',
          'layout': {
            'icon-image': 'custom-marker',
            'icon-rotation-alignment': 'map',
            // get the year from the source's "year" property
            'text-field': '{name} {count}',
            'text-rotation-alignment': 'map',
            'text-font': [
              'Open Sans Semibold',
              'Arial Unicode MS Bold'
            ],
            'text-offset': [0, 2],
            'text-anchor': 'top'
          }
        });

        setTimeout(() => {
          map.flyTo({
            center: [6.085985185235046, 52.64084840673854],
            pitch: 70,
            zoom: 16,
            duration: 2000,
            essential: true
          })
        }, 5000);
      });
  }
}
