import { Component, OnInit } from '@angular/core';
import maplibregl from 'maplibre-gl'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'maplibre-vector-tiles';

  ngOnInit() {
    const map = new maplibregl.Map({
      container: 'map',
      style: '/assets/osm_liberty.json', // stylesheet location
      center: [5.2793703, 52.2129919], // starting position [lng, lat]
      zoom: 9, // starting zoom
      minZoom: 7
    });
    
    const element = document.createElement('div');
    element.textContent = 'Foutmelding';
    element.style.fontSize = '26pt';
    element.addEventListener('click', () => {
      console.log('Gebruikelijk zou je nu naar de detail pagina worden verstuurd.')
    });

    const marker = new maplibregl.Marker({ element })
      .setLngLat([5.2793703, 52.2129919])
      .addTo(map);
  }
}
