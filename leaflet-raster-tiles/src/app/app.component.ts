import { Component, OnInit } from '@angular/core';
import { Map, TileLayer, Marker, DivIcon } from 'leaflet';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  title = 'leaflet-raster-tiles';

  ngOnInit(): void {
    const map = new Map('map').setView([52.2129919, 5.2793703], 9);

    new TileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution:
        '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
    }).addTo(map);

    const container = document.createElement('div');
    container.style.transform = 'translate(-50%, 0)';

    const element = document.createElement('div');
    element.style.width = '40px';
    element.style.height = '40px';
    element.style.backgroundColor = 'black';
    element.style.borderRadius = '20px';

    const text = document.createElement('p');
    text.style.fontSize = '18pt';
    text.textContent = 'DCEM-system';

    container.appendChild(element);
    container.appendChild(text);

    const divIcon = new DivIcon({ html: container });

    new Marker([52.2129919, 5.2793703], { icon: divIcon }).addTo(map);
  }
}
