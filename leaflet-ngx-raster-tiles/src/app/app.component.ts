import { Component, OnInit } from '@angular/core';
import {
  circle,
  DivIcon,
  latLng,
  Layer,
  marker,
  polygon,
  tileLayer,
} from 'leaflet';

@Component({
  selector: 'app-root',
  template: `
    <div
      id="map"
      leaflet
      [leafletOptions]="options"
      [leafletLayers]="layers"
    ></div>
  `,
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnInit {
  title = 'leaflet-ngx-raster-tiles';
  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '...',
      }),
    ],
    zoom: 10,
    center: latLng(52.2129919, 5.2793703),
  };
  layers: Layer[] = [];

  ngOnInit(): void {
    const container = document.createElement('div');
    container.style.transform = 'translate(-50%, 0)';

    const element = document.createElement('div');
    element.style.width = '40px';
    element.style.height = '40px';
    element.style.backgroundColor = 'black';
    element.style.borderRadius = '20px';

    const text = document.createElement('p');
    text.style.fontSize = '18pt';
    text.textContent = 'DCEM-system';

    container.appendChild(element);
    container.appendChild(text);

    const divIcon = new DivIcon({ html: container });

    this.layers = [marker([52.2129919, 5.2793703], { icon: divIcon })];
  }
}
